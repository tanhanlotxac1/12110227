Phạm Bình Trọng     MSSV: 12110207
Website: review về các nhà hàng, quán ăn đồng thời cung cấp các thông tin về ẩm thực trong nước cũng như trên
thế giới. Đem tới cho người truy cập những thông tin khuyến mãi từ các nhà hàng, quán ăn, những địa chỉ ẩm 
thực đang được nhiều người quan tâm.
Lý do chọn Website:
-Lĩnh vực ẩm thực sôi động và có nhiều cơ hội, được nhiều người  quan tâm, không những vậy, trang web đưa ra nhiều địa điểm ẩm thực nổi tiếng, ngon miệng, đáng tin cậy.
-Cung cấp những thông tin nhanh, chính xác, đáng tin cậy, giúp người dùng có thể nắm bắt được những địa điểm ăn rẻ và hấp dẫn.
-Hơn nữa, Website luôn cập những sự kiện khuyến mãi, combo đang hạ giá cho người dùng để người dùng có thể trải nghiệm một cách thoải mái, thuận tiện và tiết kiệm chi phí
-Website còn review các món ăn trong nước và nước ngoài, các quán ăn nhà hàng nổi tiếng và mới mở, thái độ phục vụ, chất lượng các món ăn một cách khách quan, trung thực
-Ngoài ra, trang web củng có những mẹo vặt dành cho người dùng thích du lịch để tránh khỏi sự "chặt chém" của các quán ăn, nhà hàng, và những tips nấu ăn hay, dễ áp dụng.
-Cung cấp những tin tức mới về lĩnh vực ẩm thực, xu hướng ẩm thực trong nước và trên thê giới.
-Với chức năng bình luận sau các bài review, người dùng có thể đưa ra ý kiến của mình, làm bài review có thêm được cái nhìn khách quan
-Hệ thống Forum hiệu quả, với chế độ thăng cấp bằng cách cộng điểm thú vị, người dùng có thể chia sẽ, thảo luận về những tin tức ẩm thực

