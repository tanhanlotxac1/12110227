﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class Account
    {
        public int ID { set; get; }
        [Required] 
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [DataType(DataType.EmailAddress)]
        [Required] 
        public String Email { set; get; }

        [Required]
        [StringLength(250, ErrorMessage = "So luong ky tu trong khoang 10 - 250", MinimumLength = 10)]
        public String FirstName { set; get; }

        [Required]
        [StringLength(250, ErrorMessage = "So luong ky tu trong khoang 10 - 250", MinimumLength = 10)]
        public String LastName { set; get; }

        public ICollection<Post> Posts { set; get; }
    }
}